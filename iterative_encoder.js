"use strict";

const Coder = require('./abstract_coder.js');

module.exports =  class Encoder extends Coder{
    constructor(arrOfBytes){
        super(arrOfBytes);
    }

    encode(row, col){
        this._row = row;
        this._col = col;
        this._codeArr = this._createArr(row, col);
        let currentByte = 0;
        let mask = 0x80;
        let chk = false;
        while(1) {
            this._codeArr = this._createArr(row, col);
            for (let i = 0; i < row; ++i) {
                for (let j = 0; j < col; ++j) {
                    this._codeArr[i][j] = (this._bytes[currentByte] & mask) ? 1 : 0;
                    if (mask == 0x1) {
                        if (this._bytes.length == ++currentByte) {
                            this._addParityBit();
                            this._writeToResult();
                            chk = true;
                            break;
                        }
                        mask = 0x100;
                    }
                    mask /= 2;
                }
                if(chk) break;
            }
            if(chk) break;
            this._addParityBit();
            this._writeToResult();
        }
        if(this._writeMask != 0x80){
            this._result.push(this._writeByte);
        }
        return this._result;
    }


    _addParityBit(){
        for(let i = 0; i < this._row; ++i){
            this._codeArr[i][this._col] = this._codeArr[i].reduce( (acc, cur, idx) => {
                if(idx == this._col) return acc;
                return acc ^ cur;
            } );
        }

        for(let i = 0; i < this._col + 1; ++i){
            this._codeArr[this._row][i] = ( () => {
                let acc = 0;
                for(let j = 0; j < this._row; ++j){
                    acc ^= this._codeArr[j][i];
                }
                return acc;
            } )();
        }

        this._codeArr[this._row][this._col] = this._codeArr[this._row].reduce( (acc, cur) =>{
            return acc^cur;
        } );
    }

};