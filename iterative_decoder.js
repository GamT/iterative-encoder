"use strict";

const Coder = require("./abstract_coder.js");

module.exports = class Decoder extends Coder{
    constructor(arrOfBytes){
        super(arrOfBytes);
        this._foundBrokenPacket = 0;
        this._fixedPackets = 0;
    }

    decode(row ,col){
        this._row = row;
        this._col = col;
        this._foundBrokenPacket = 0;
        this._fixedPackets = 0;
        let currentByte = 0;
        let mask = 0x80;
        let chk = false;

        while (1){
            this._codeArr = this._createArr(row, col);
            for (let i = 0; i < row + 1; ++i) {
                for (let j = 0; j < col + 1; ++j) {
                    this._codeArr[i][j] = (this._bytes[currentByte] & mask) ? 1 : 0;
                    if (mask == 0x1) {
                        if (this._bytes.length == ++currentByte) {
                            this._checkErrors();
                            this._removeParityBit();
                            this._writeToResult();
                            chk = true;
                            break;
                        }
                        mask = 0x100;
                    }
                    mask /= 2;
                }
                if(chk) break;
            }
            if(chk) break;
            this._checkErrors();
            this._removeParityBit();
            this._writeToResult();
        }
        if(this._writeMask != 0x80){
            this._result.push(this._writeByte);
        }
        while(this._result[this._result.length -1] === 0){
            this._result.pop();
        }
        return this._result;
    }

    _checkErrors(){
        let rowErr = [];
        this._codeArr.forEach( (item, idx) => {
            if(item.reduce( (acc, cur) => {
                if(idx === this._row){
                    return false;
                }
                    return acc ^ cur;
                } )){
                rowErr.push(idx);
            }
        } );

        let colErr = [];
        for(let i = 0; i < this._col; ++i){
            let acc = 0;
            for(let j = 0; j < this._codeArr.length; ++j){
                acc ^= this._codeArr[j][i];
            }
            if(acc){
                colErr.push(i);
            }
        }

        let crossErr = 0;
        crossErr = this._codeArr[this._row].reduce( (acc, cur) =>{
            return acc^cur;
        } );

        for(let i = 0; i < this._row + 1; ++i){
            crossErr ^= this._codeArr[i][this._col];
        }

        if(rowErr.length > 0 || colErr.length > 0 || crossErr){
            this._foundBrokenPacket++;
            if(rowErr.length == 1 && colErr.length == 1){
                this._codeArr[rowErr[0]][colErr[0]] = !this._codeArr[rowErr[0][colErr[0]]];
                this._fixedPackets++;
            }else if(rowErr.length === 0 && colErr.length === 0 && crossErr){
                this._fixedPackets++;
            }

        }
    }

    _removeParityBit(){
        let arr = this._createArr(this._row - 1, this._col - 1);
        arr.forEach( (item, i) => {
            item.forEach( (value, j) => {
                arr[i][j] = this._codeArr[i][j];
            } );
        } );
        this._codeArr = arr;
    }

    get foundErrors(){
        return this._foundBrokenPacket;
    }

    get fixedErrors(){
        return this._fixedPackets;
    }
};