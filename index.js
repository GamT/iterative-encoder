"use strict";

const Encoder = require('./iterative_encoder.js');
const Decoder = require('./iterative_decoder.js');
const readline = require('readline-sync');

const fs = require('fs');

let filePath;
let inputData;
let code;
let outStr;
while(1) {
    filePath = null;
    inputData = null;
    code = null;
    outStr = "";

    let isEncode = whatToDo();

    fileRead();

    let arr = [];
    [].forEach.call(inputData, (item) => arr.push(item.charCodeAt(0)));

    let row = +readline.questionInt("Enter number of rows: ");
    let col = +readline.questionInt("Enter number of cols: ");

    let coder = (isEncode) ? new Encoder(arr) : new Decoder(arr);
    if(isEncode){
        arr = coder.encode(row,col);
    }else{
        arr = coder.decode(row,col);
        console.log(`Found errors = ${coder.foundErrors}`);
        console.log(`Fixed errors = ${coder.fixedErrors}`);
    }

    arr.forEach((char) => {
        outStr += String.fromCharCode(char);
    });

    fileWrite();
}


//functions

function fileRead() {
    try {
        filePath =  readline.question("Enter file path name: ");
        inputData = fs.readFileSync(filePath, 'latin1');
    }catch (e) {
        console.log(`Wrong file name "${filePath}"`);
        fileRead();
    }
}

function fileWrite() {
    try {
        filePath = readline.question("Enter output file name: ");
        fs.writeFileSync(filePath, outStr, 'latin1');
    }catch (e) {
        console.log(`Cannot open output file "${filePath}"`);
        fileWrite();
    }
}

function whatToDo(){
    code = readline.question("Encode or Decode? ");
    code = code.toLowerCase();
    switch (code) {
        case "encode":
            return true;
        case "decode":
            return  false;
        case "quit":
        case "exit":
            process.exit(0);
        default:
            return whatToDo();
    }
}