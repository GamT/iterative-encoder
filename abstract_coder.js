"use strict";

module.exports = class Coder{
    constructor(arrOfBytes){
        if(this.constructor.name === 'Coder'){
            throw new Error(`${this.constructor.name}: can not create instance of abstract class`);
        }

        this._bytes = arrOfBytes;
        this._writeMask = 0x80;
        this._writeByte = 0;
        this._codeArr = [];
        this._row = 0;
        this._col = 0;
        this._result = [];
    }

    _createArr(row, col){
        let arr = [];
        for(let i = 0; i < row + 1; ++i){
            arr[i] = [];
            for(let j = 0; j < col + 1; ++j){
                arr[i][j] = 0;
            }
        }
        return arr;
    }

    _writeToResult(){
        this._codeArr.forEach( (item) => {
            item.forEach( (value) => {
                this._writeByte += ( () => {
                    for(let i = 1; i < this._writeMask; i *= 2){
                        value *= 2;
                    }
                    return value;
                } )();
                if(this._writeMask == 1){
                    this._result.push(this._writeByte);
                    this._writeByte = 0;
                    this._writeMask = 0x100;
                }
                this._writeMask /= 2;
            } );
        } );
    }
};